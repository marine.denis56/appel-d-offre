package com.bricoplus.Bricoplus_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.bricoplus.Bricoplus_api")
public class BricoplusApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BricoplusApiApplication.class, args);
	}

}

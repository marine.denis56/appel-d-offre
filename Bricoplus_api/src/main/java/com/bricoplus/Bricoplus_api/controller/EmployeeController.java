package com.bricoplus.Bricoplus_api.controller;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import com.bricoplus.Bricoplus_api.model.Employee;
import com.bricoplus.Bricoplus_api.model.EmployeeAttendanceDTO;
import com.bricoplus.Bricoplus_api.service.EmployeeService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public EmployeeController(EmployeeService employeeSrv) {
        this.employeeService = employeeSrv;
    }

    @GetMapping(path = "/employees", produces = "application/json")
    public Iterable<Employee> getAllEmployees() {
        return employeeService.findAll();
    }

    @GetMapping(path = "/employee/{id}", produces = "application/json")
    public Employee getEmployeeById(@PathVariable long id) {
        return employeeService.findById(id);
    }

    @PostMapping(path = "/register")
    public void registerEmployee(@RequestBody Employee employee) {
        System.out.println(employee.getPassword());
        employee.setPassword(passwordEncoder.encode(employee.getPassword()));
        employeeService.save(employee);
    }

    @GetMapping(path = "/employee/date", produces = "application/json")
    public List<Employee> getEmployeeByDate(@RequestParam String date) {
        return employeeService.getByDate(date);
    }

    @GetMapping(path = "/employee/attendances", produces = "application/json")
    public List<EmployeeAttendanceDTO> getMethodName(@RequestParam Date date) {
        System.out.println(date);
        return employeeService.getAttendanceByDate(date, "09:00:00");
    }

}

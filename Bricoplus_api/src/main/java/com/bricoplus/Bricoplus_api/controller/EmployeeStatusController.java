package com.bricoplus.Bricoplus_api.controller;

import java.util.List;
import java.sql.Date;

import org.springframework.web.bind.annotation.*;

import com.bricoplus.Bricoplus_api.model.EmployeeStatus;
import com.bricoplus.Bricoplus_api.service.EmployeeStatusService;

@RestController
@CrossOrigin(origins = "*")
public class EmployeeStatusController {

    private EmployeeStatusService employeeStatusService;

    public EmployeeStatusController(EmployeeStatusService employeeStatusSrv) {
        this.employeeStatusService = employeeStatusSrv;
    }

    @GetMapping(path = "/api/employee/status", produces = "application/json")
    public Iterable<EmployeeStatus> getAllEmployeeStatus() {
        return employeeStatusService.findAll();
    }

    @GetMapping(path = "/api/employee/status/{id}", produces = "application/json")
    public EmployeeStatus getEmployeeStatusById(@PathVariable long id) {
        return employeeStatusService.findById(id);
    }

    @GetMapping(path = "/api/employee/status/attendance/{idEmployee}", produces = "application/json")
    public List<EmployeeStatus> getAttendenceByIdEmployee(@PathVariable long idEmployee) {
        return employeeStatusService.getByIdEmployee(idEmployee);
    }

    @PostMapping(path = "/api/employee/status/create")
    public void createEmployeeStatus(@RequestBody EmployeeStatus employeeStatus) {
        employeeStatusService.save(employeeStatus);
    }
}

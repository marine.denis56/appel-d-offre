package com.bricoplus.Bricoplus_api.model;

import java.io.Serializable;
import java.util.Objects;

import jakarta.persistence.*;

@Entity
@Table(name = "employee")
public class Employee implements Serializable {

  @Id()
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "phone")
  private String phone;

  @Column(name = "departement")
  private String departement;

  @Column(name = "role")
  private String role;

  @Column(name = "gender")
  private String gender;

  @Column(name = "email")
  private String email;

  @Column(name = "password")
  private String password;

  // getters and setters, hashcode, equals, toString etc. are omitted

  public Employee() {}

  public Employee(String firstName, String lastName, String phone, String departement, String role, String gender, String email, String password) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.phone = phone;
    this.departement = departement;
    this.role = role;
    this.gender = gender;
    this.email = email;
    this.password = password;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Long getId() {
    return this.id;
  }

  public String getFirstName() {
    return this.firstName;
  }

  public String getLastName() {
    return this.lastName;
  }

  public String getRole() {
    return this.role;
  }

  public String getGender() {
    return this.gender;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getDepartement() {
    return this.departement;
  }

  public String getPhone() {
    return phone;
  }

  public void setDepartement(String departement) {
    this.departement = departement;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  @Override
  public boolean equals(Object o) {

    if (this == o)
      return true;
    if (!(o instanceof Employee))
      return false;
    Employee employee = (Employee) o;
    return Objects.equals(this.id, employee.id) && Objects.equals(this.firstName,
        employee.firstName)
        && Objects.equals(this.role, employee.role);
  }

  @Override
  public String toString() {
    return "Employee{" + "id=" + this.id + ", name='" + this.firstName + '\'' + ", role='" + this.role + '\'' + '}';
  }
}
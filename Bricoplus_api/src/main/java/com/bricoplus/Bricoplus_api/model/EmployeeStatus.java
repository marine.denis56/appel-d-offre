package com.bricoplus.Bricoplus_api.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

import jakarta.persistence.*;

@Entity
@Table(name = "employee_status")
public class EmployeeStatus implements Serializable {

    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "id_employee")
    private Long idEmployee;

    @Column(name = "date")
    private Date date;

    @Column(name = "time_in")
    private String timeIn;

    public EmployeeStatus() {
    }

    public EmployeeStatus(Long idEmployee, Date date, String timeIn) {

        this.idEmployee = idEmployee;
        this.date = date;
        this.timeIn = timeIn;
    }

    public Long getId() {
        return this.id;
    }

    public Long getIdEmployee() {
        return this.idEmployee;
    }

    public Date getDate() {
        return this.date;
    }

    public String getTimeIn() {
        return this.timeIn;
    }

    public void setIdEmployee(Long idEmployee) {
        this.idEmployee = idEmployee;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setTimeIn(String timeIn) {
        this.timeIn = timeIn;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o)
            return true;
        if (!(o instanceof EmployeeStatus))
            return false;
        EmployeeStatus employeeStatus = (EmployeeStatus) o;
        return Objects.equals(this.id, employeeStatus.id) && Objects.equals(this.idEmployee,
                employeeStatus.idEmployee);
    }

    @Override
    public String toString() {
        return "EmployeeStatus{" + "id=" + this.id + ", date='" + this.idEmployee + '\'' + ", date='" + this.date + '\''
                + '}';
    }
}
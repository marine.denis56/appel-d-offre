package com.bricoplus.Bricoplus_api.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import org.springframework.stereotype.Repository;

import com.bricoplus.Bricoplus_api.model.Employee;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    Optional<Employee> findByEmail(String email);

    @Query(value = "SELECT * FROM employee WHERE employee.role=:role", nativeQuery = true)
    List<Employee> findAllByRole(String role);
}
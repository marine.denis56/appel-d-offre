package com.bricoplus.Bricoplus_api.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.bricoplus.Bricoplus_api.model.EmployeeStatus;

@Repository
public interface EmployeeStatusRepository extends CrudRepository<EmployeeStatus, Long> {

    @Query(value = "SELECT * FROM employee_status WHERE employee_status.date=:chosenDate", nativeQuery = true)
    public List<EmployeeStatus> getByDate(Date chosenDate);

    @Query(value = "SELECT * FROM employee_status WHERE employee_status.id_employee=:idEmployee", nativeQuery = true)
    public List<EmployeeStatus> getByIdEmployee(Long idEmployee);
}

package com.bricoplus.Bricoplus_api.service;

import com.bricoplus.Bricoplus_api.model.Employee;
import com.bricoplus.Bricoplus_api.model.UserDetailsImpl;
import com.bricoplus.Bricoplus_api.repository.EmployeeRepository;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final EmployeeRepository employeeRepository;

    public CustomUserDetailsService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Employee user = employeeRepository.findByEmail(email)
                .orElseThrow(() ->
                        new UsernameNotFoundException("User with email [ "+ email +" ] not found."));


        return new UserDetailsImpl(user.getId(),user.getEmail(),
                user.getPassword(), Collections.singleton(new SimpleGrantedAuthority(user.getRole())));
    }
}
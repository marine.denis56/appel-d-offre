package com.bricoplus.Bricoplus_api.service;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.stereotype.Service;

import com.bricoplus.Bricoplus_api.model.Employee;
import com.bricoplus.Bricoplus_api.model.EmployeeAttendanceDTO;
import com.bricoplus.Bricoplus_api.model.EmployeeStatus;
import com.bricoplus.Bricoplus_api.repository.EmployeeRepository;

@Service
public class EmployeeService {

    private EmployeeRepository employeeRepo;
    private EmployeeStatusService statusService;

    public EmployeeService(EmployeeRepository repo, EmployeeStatusService statusSrv) {
        this.employeeRepo = repo;
        this.statusService = statusSrv;
    }

    public void save(Employee employee) {
        employeeRepo.save(employee);
    }

    public Employee findById(Long id) {
        return employeeRepo.findById(id).orElse(null);
    }

    public Iterable<Employee> findAll() {
        return employeeRepo.findAllByRole("employee");
    }

    public List<Employee> getByDate(String date) {
        List<EmployeeStatus> statusList = statusService.getByDate(date);

        List<Employee> employeesByDate = new ArrayList<>();
        for (EmployeeStatus status : statusList) {
            employeesByDate.add(findById(status.getIdEmployee()));
        }

        return employeesByDate;
    }

    public List<EmployeeAttendanceDTO> getAttendanceByDate(Date chosenDate, String startHourString) {

        // Format pour le parsing
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");

        // Convertir les chaînes en objets LocalTime
        LocalTime startHour = LocalTime.parse(startHourString, formatter);

        List<EmployeeAttendanceDTO> todaysAttendances = new ArrayList<>();

        // Pour chaque employé
        for (Employee employee : findAll()) {
            EmployeeStatus employeeAttendance = statusService.getByIdEmployee(employee.getId()).size() > 0
                    ? statusService.getByIdEmployee(employee.getId()).get(0)
                    : null;
            EmployeeAttendanceDTO employeeAttendanceDTO = new EmployeeAttendanceDTO();
            employeeAttendanceDTO.setEmployeeId(employee.getId());
            employeeAttendanceDTO.setFullName(employee.getFirstName() + " " + employee.getLastName());
            employeeAttendanceDTO.setGender(employee.getGender());

            if (Objects.isNull(employeeAttendance)) {
                employeeAttendanceDTO.setStatus("absent");
            }

            else {
                employeeAttendanceDTO.setDate(chosenDate);
                employeeAttendanceDTO.setHour(employeeAttendance.getTimeIn());

                // On compare les heures d'arrivées à l'heure contractuelle
                if (LocalTime.parse(employeeAttendance.getTimeIn(), formatter).isBefore(startHour)) {
                    employeeAttendanceDTO.setStatus("onTime");
                } else if (LocalTime.parse(employeeAttendance.getTimeIn(), formatter).isAfter(startHour)) {
                    employeeAttendanceDTO.setStatus("late");
                } else {
                    employeeAttendanceDTO.setStatus("absent");
                }
            }
            todaysAttendances.add(employeeAttendanceDTO);
        }

        return todaysAttendances;
    }

}
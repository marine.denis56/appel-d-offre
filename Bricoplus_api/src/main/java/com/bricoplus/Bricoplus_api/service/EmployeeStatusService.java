package com.bricoplus.Bricoplus_api.service;

import java.sql.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.bricoplus.Bricoplus_api.model.EmployeeStatus;
import com.bricoplus.Bricoplus_api.repository.EmployeeStatusRepository;

@Service
public class EmployeeStatusService {

    private EmployeeStatusRepository employeeStatusRepo;

    public EmployeeStatusService(EmployeeStatusRepository repo) {
        this.employeeStatusRepo = repo;
    }

    public void save(EmployeeStatus employeeStatus) {
        employeeStatusRepo.save(employeeStatus);
    }

    public EmployeeStatus findById(Long id) {
        return employeeStatusRepo.findById(id).orElse(null);
    }

    public Iterable<EmployeeStatus> findAll() {
        return employeeStatusRepo.findAll();
    }

    public List<EmployeeStatus> getByDate(String date) {
        return employeeStatusRepo.getByDate(Date.valueOf(date));
    }

    public List<EmployeeStatus> getByIdEmployee(Long id) {
        return employeeStatusRepo.getByIdEmployee(id);
    }
}
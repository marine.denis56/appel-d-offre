# Bricoplus


## Frontend
- install dependencies : npm install
- run : npm run dev

## Backend

### Database
- install mysql-server
- connect to mysql -> mysql -u USERNAME -p (replace USERNAME with your mysql username)
- run sql script -> source bdd.sql 

Run API : ./mvnw spring-boot:run (in Bricoplus_api/ directory)

### Endpoints

Créer un employé : GET /api/register 

Get un employé : GET /api/employee/{id}

Get tous les employés : GET /api/employees

Get tous les employés par date : GET /api/employees/date?date=xx-xx-xxxx

Get tous les statuts d'une date : GET /api/employee/attendances?date

Get tous les statut des employés par date : GET /api/employees/attendance?date=xx-xx-xxxx

Get tous les statuts d'un employé : /api/employee/status/attendance/{idEmployee}

Pointer en tant qu'employé : POST /api/employee/status/create

Login : POST /signin



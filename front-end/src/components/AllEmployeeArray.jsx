import classNames from "classnames"
import Image from "next/image"

const TableComponent = (props) => {
  const { classNameDiv, classNameNb, img, alt, title, nb } = props

  return (
    <div>
      <p className="text-semi-bold text-center">{title}</p>
      <div
        className={classNames(
          "w-[214px] h-[100px] bg-white flex justify-center items-center border border-grey",
          classNameDiv
        )}
      >
        <Image src={img} alt={alt} width={70} height={30} />
        <p className={classNames("text-bold text-4xl", classNameNb)}>{nb}</p>
      </div>
    </div>
  )
}

const AllEmployeeArray = (props) => {
  const { late, onTime, absent } = props

  return (
    <div className="flex justify-center">
      <TableComponent
        classNameDiv="rounded-l-lg"
        classNameNb="text-black"
        img="/black.png"
        alt="all"
        title="Total employés"
        nb={late + onTime + absent}
      />
      <TableComponent
        classNameNb="text-red"
        img="/red.png"
        alt="absent"
        title="Absents"
        nb={absent}
      />
      <TableComponent
        classNameNb="text-yellow"
        img="/yellow.png"
        alt="late"
        title="Retards"
        nb={late}
      />
      <TableComponent
        classNameDiv="rounded-r-lg"
        classNameNb="text-green"
        img="/green.png"
        alt="onTime"
        title="À l'heure"
        nb={onTime}
      />
    </div>
  )
}

export default AllEmployeeArray

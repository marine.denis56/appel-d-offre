import clsx from "clsx"
const colors = {
    primary:
      "rounded-2xl bg-dark-yellow text-black",
}
  

const sizes = {
    md: "text-[48px] font-semibold	 w-[417px] h-[116px]",
  }
  
  const Button = (props) => {
    const { color = "primary", size = "md", className, ...otherProps } = props
  
    return (
      <button
        className={clsx(colors[color], className, sizes[size])}
        {...otherProps}
      />
    )
  }
  
  export default Button
  
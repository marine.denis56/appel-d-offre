import classNames from "classnames"
import Image from "next/image"
import { useEffect, useState } from "react"

const EmployeeStatus = (props) => {
  const { status } = props
  const [color, setColor] = useState("bg-red")
  const [statusString, setStatusString] = useState("absent")

  useEffect(() => {
    if (status === "onTime") {
      setColor("bg-green")
      setStatusString("à l'heure")
    }

    if (status === "late") {
      setColor("bg-yellow")
      setStatusString("en retard")
    }
  })

  return (
    <div
      className={classNames(
        color,
        "w-[151px] h-[53px] mr-[110px] rounded-full"
      )}
    >
      <p className="flex p-3 justify-center">{statusString}</p>
    </div>
  )
}

const EmployeeDetails = (props) => {
  const { fullName, gender, status, time } = props

  return (
    <div className="border-t-4 border-dark-grey">
      <div className="p-[30px] flex">
        <div className="flex items-center">
          <div className="bg-black w-[156px] h-[156px] mr-20 rounded-full flex justify-center relative">
            {gender === "F" ? (
              <Image
                src={"/woman.png"}
                alt={"woman"}
                layout="fill"
                className="p-5"
              />
            ) : (
              <Image
                src={"/man.png"}
                alt={"man"}
                layout="fill"
                className="p-5"
              />
            )}
          </div>
          <p className="text-extrabold text-2xl">{fullName}</p>
        </div>
        <div className="right-[40px] absolute flex pt-[70px]">
          <EmployeeStatus status={status} />
          <p className="pr-[20px] pt-3 w-10">{time}</p>
        </div>
      </div>
    </div>
  )
}

const EmployeeArray = (props) => {
  const { allEmployees } = props

  return (
    <div className="mt-[60px]">
      {allEmployees.map((employee) => {
        return (
          <EmployeeDetails
            fullName={employee.fullName}
            gender={employee.gender}
            status={employee.status}
            time={employee.hour}
          />
        )
      })}
    </div>
  )
}

export default EmployeeArray

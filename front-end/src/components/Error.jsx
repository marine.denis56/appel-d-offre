import classNames from "classnames"

const ErrorParagraph = (props) => {
  const { messageError } = props

  return (
    <p className="text-center text-red text-xl pt-4">
      {messageError}
    </p>
  )
}

export default ErrorParagraph

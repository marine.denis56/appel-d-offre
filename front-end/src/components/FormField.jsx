import classNames from "classnames"
import { ExclamationTriangleIcon } from "@heroicons/react/24/solid"
import { Field } from "formik"

const FormField = (props) => {
  const {
    name,
    label,
    className,
    placeholder,
    replaceDefaultClassName,
    value,
    type,
    classNameInput
  } = props
  const defaultClassName = "flex flex-col gap-2"

  return (
    <Field name={name}>
      {({ field, meta }) => (
        <label
          className={classNames(
            replaceDefaultClassName
              ? replaceDefaultClassName
              : defaultClassName,
            className
          )}
        >
          <span className="text-sm font-semibold whitespace-pre-wrap">
            {label}
          </span>
          <input
            type={type || "text"}
            {...field}
            className={classNames("border-2 border-dark-yellow px-2 py-1", classNameInput)}
            placeholder={placeholder ?? label}
            value={value ?? undefined}
          />
          {meta.touched && meta.error ? (
            <span className="text-sm text-red flex gap-2 items-center">
              <ExclamationTriangleIcon className="w-6" /> {meta.error}
            </span>
          ) : null}
        </label>
      )}
    </Field>
  )
}

export default FormField

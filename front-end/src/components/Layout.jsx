import Image from "next/image"

const Layout = (props) => {
  const { className, children } = props
  return (
    <main className="bg-grey min-w-screen w-full min-h-screen h-full relative">
      <div>
        <header>
          <Image
            src="/name.png"
            alt="name"
            width={171}
            height={93}
            className="bg-none pt-[30px] pl-[30px] "
          />
        </header>
        <section>{children}</section>
      </div>
    </main>
  )
}

export default Layout

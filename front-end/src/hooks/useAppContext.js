import { createApi } from "@/services/api"
import { useCookies } from "react-cookie"
import * as loginService from "@/services/login"
import * as employeeService from "@/services/employee"
import { createContext, useContext, useEffect, useState } from "react"
import errorHandler from "@/utils/errorHandler"

export const AppContextProvider = (props) => {
  const { isPublicPage, ...otherProps } = props
  const [cookies, setCookie, removeCookie] = useCookies(["session"])

  const api = createApi({ jwt: cookies.session })

  const login = loginService.loginService({ api, setCookie, errorHandler })
  const createEmployeeStatus = employeeService.postEmployeeStatusService({
    api,
    errorHandler,
  })
  const getEmployeesByDate = employeeService.getEmployeesByDateService({
    api,
    errorHandler,
  })

  const createEmployee = employeeService.createEmployeeService({
    api,
    errorHandler,
  })

  if (!isPublicPage && cookies.session === null) {
    return (
      <div className="fixed inset-0 bg-white z-1000 flex items-center justify-center h-screen w-screen">
        <p>
          Your not connected, please go to the{" "}
          <a href="/" className="text-yellow">
            login page
          </a>
        </p>
      </div>
    )
  }

  return (
    <AppContext.Provider
      {...otherProps}
      value={{
        state: {
          cookies,
        },
        actions: {
          login,
          createEmployeeStatus,
          getEmployeesByDate,
          createEmployee,
        },
      }}
    />
  )
}

const AppContext = createContext()
const useAppContext = () => useContext(AppContext)

export default useAppContext

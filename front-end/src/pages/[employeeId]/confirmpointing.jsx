import Layout from "@/components/Layout"
import { useState, useEffect } from "react"
import { useRouter } from "next/router"
import useAppContext from "@/hooks/useAppContext"
import parseSession from "@/utils/parseSession"

export const getServerSideProps = async (context) => {
  const { employeeId } = context.params
  return { props: { employeeId: employeeId } }
}

const Pointing = (props) => {
  const { employeeId } = props
  const router = useRouter()
  const { currentHour, formattedDate } = router.query

  const {
    state: { cookies },
  } = useAppContext()

  useEffect(() => {
    if (parseSession(cookies.session).roles[0].authority === "rh") {
      router.push("/rhview")
    }
  })

  return (
    <Layout>
      <div className="flex justify-center mt-[11rem]">
        <div className=" flex flex-col justify-center  items-center bg-yellow w-[745px] h-[284px] rounded-2xl">
          <p className="font-extrabold text-[48px]">
            Vous êtes noté présent le:
          </p>
          <div className="flex flex-col justify-center items-center mt-2">
            <h5 className="font-extrabold text-[48px]">{formattedDate}</h5>
            <h5 className="font-extrabold text-[48px]"> {currentHour}</h5>
          </div>
        </div>
      </div>
    </Layout>
  )
}
export default Pointing

import Button from "@/components/Button"
import Layout from "@/components/Layout"
import { useState, useEffect, useCallback } from "react"
import { useRouter } from "next/router"
import { createApi } from "@/services/api"
import errorHandler from "@/utils/errorHandler"
import { getEmployeeService } from "@/services/employee"
import useAppContext from "@/hooks/useAppContext"
import parseSession from "@/utils/parseSession"

export const getServerSideProps = async (context) => {
  const { employeeId } = context.params
  const api = createApi({ jwt: context.req.cookies.session })

  const getEmployee = getEmployeeService({ api, errorHandler })
  const [error, data] = await getEmployee(employeeId)

  if (error) {
    return { props: { error: error } }
  }

  return {
    props: { employee: data },
  }
}

const Pointing = (props) => {
  const { error, employee } = props

  const [formattedDate, setFormattedDate] = useState("")
  const [currentHour, setCurrentHour] = useState("")
  const router = useRouter()

  const {
    actions: { createEmployeeStatus },
    state: { cookies },
  } = useAppContext()

  useEffect(() => {
    if (parseSession(cookies.session).roles[0].authority === "rh") {
      router.push("/rhview")
    }

    if (parseSession(cookies.session).id !== employee.id) {
      router.push(`/${parseSession(cookies.session).id}/pointing`)
    }

    const getCurrentDate = () => {
      const date = new Date()
      const options = { weekday: "long", month: "long", day: "numeric" }
      const formattedDate = date.toLocaleDateString("fr-FR", options)
      const currentHour = date.toLocaleTimeString("fr-FR", {
        hour: "2-digit",
        minute: "2-digit",
      })
      setFormattedDate(formattedDate)
      setCurrentHour(currentHour)
    }

    getCurrentDate()
    const intervalId = setInterval(getCurrentDate, 60000)
    return () => clearInterval(intervalId)
  }, [])

  const handleSubmit = useCallback(async () => {
    await createEmployeeStatus(employee.id, new Date().getTime(), currentHour)

    router.push({
      pathname: `/${employee.id}/confirmpointing`,
      query: { currentHour: currentHour, formattedDate: formattedDate },
    })
  }, [currentHour, formattedDate])
  return (
    <Layout>
      <div className="flex justify-center ">
        <div className="rounded-full bg-dark-yellow w-[193px] h-[193px] z-10 flex flex-col justify-center items-center -mr-[50px] mt-6">
          <p className="font-extrabold text-[32px]">{employee.firstName}</p>
          <p className="font-extrabold text-[32px]">{employee.lastName}</p>
        </div>
        <div className=" flex flex-col justify-between bg-yellow z-0 w-[745px] h-[662px]">
          <div className="flex flex-col justify-center items-center pt-8">
            <h5 className="font-extrabold text-[48px]">{formattedDate}</h5>
            <h5 className="font-extrabold text-[48px]"> {currentHour}</h5>
          </div>
          <div className="flex justify-center items-center p-[5.5rem]">
            <Button onClick={handleSubmit}>Pointer</Button>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default Pointing

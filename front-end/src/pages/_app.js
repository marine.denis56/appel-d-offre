import { AppContextProvider } from "@/hooks/useAppContext"
import "@/styles/globals.css"
import { CookiesProvider } from "react-cookie"

export default function AppContext({ Component, pageProps }) {
  return (
    <CookiesProvider>
      <AppContextProvider isPublicPage={Component.isPublic}>
        <Component {...pageProps} />
      </AppContextProvider>
    </CookiesProvider>
  )
}

import ErrorParagraph from "@/components/Error"
import FormField from "@/components/FormField"
import Layout from "@/components/Layout"
import useAppContext from "@/hooks/useAppContext"
import { Field, Form, Formik } from "formik"
import { useRouter } from "next/router"
import { useCallback, useState } from "react"
import * as yup from "yup"

const RadioField = (props) => {
  const { values, name } = props

  return (
    <div className="flex gap-6">
      {values.map((value) => {
        return (
          <FormField
            name={name}
            replaceDefaultClassName="flex gap-2"
            type="radio"
            value={value.value}
            label={value.label}
          />
        )
      })}
    </div>
  )
}

const AddEmployee = () => {
  const [err, setErr] = useState(null)
  const router = useRouter()
  const {
    actions: { createEmployee },
  } = useAppContext()

  const handleSubmit = useCallback(
    async (submitting) => {
      setErr(null)

      const [error] = await createEmployee(
        submitting.firstName,
        submitting.lastName,
        submitting.gender,
        submitting.role,
        submitting.departement,
        submitting.phone,
        submitting.email,
        submitting.password
      )

      if (error) {
        //gestion de l'erreur
        setErr(error)

        return
      }

      router.push("/rhview")
    },
    [router] //ajout service
  )

  const initialValues = {
    gender: "",
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    role: "",
    phone: "",
    departement: "",
  }

  const validationSchema = yup.object().shape({
    gender: yup.string().required("Genre manquant"),
    //firstName: yup.string().required("Prénom manquant"),
    lastName: yup.string().required("Nom manquant"),
    email: yup.string().required("Email manquant").email("Email invalide"),
    password: yup
      .string()
      .required("Mot de passe manquant")
      .min(12, "Le mot de passe doit faire au moins 12 caractères")
      .matches(
        /[a-z]/,
        "Le mot de passe doit contenir au moins une lettre minuscule"
      )
      .matches(
        /[A-Z]/,
        "Le mot de passe doit contenir au moins une lettre majuscule"
      )
      .matches(/[0-9]/, "Le mot de passe doit contenir au moins un chiffre")
      .matches(
        /[@$!%*#?&]/,
        "Le mot de passe doit contenir au moins un caractère spécial"
      ),
    role: yup.string().required("Role manquant"),
    phone: yup
      .string()
      .required("Téléphone manquant")
      .matches(/(?:[0-9]●?){6,14}[0-9]/, "Téléphone invalide"),
    departement: yup.string().required("Département manquant"),
  })

  const valuesGender = [
    {
      id: "genderM",
      value: "M",
      label: "M",
    },
    {
      id: "genderF",
      value: "F",
      label: "Mme",
    },
  ]

  const valuesRole = [
    {
      id: "employee",
      value: "employee",
      label: "Employé(e)",
    },
    {
      id: "rh",
      value: "rh",
      label: "RH",
    },
  ]

  return (
    <Layout>
      <h1 className="text-center pt-[150px] font-bold text-4xl">
        Ajouter un employé
      </h1>
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
        error={err}
      >
        {({ isSubmitting, isValid }) => (
          <Form className="flex flex-col items-center gap-4 p-4">
            <RadioField values={valuesGender} name="gender" />
            <FormField name="firstName" label="Prénom" className="w-80" />
            <FormField name="lastName" label="Nom" className="w-80" />
            <FormField name="email" label="Email" className="w-80" />
            <FormField name="password" label="Mot de passe" className="w-80" />
            <RadioField values={valuesRole} name="role" />
            <FormField name="phone" label="Téléphone" className="w-80" />
            <FormField
              name="departement"
              label="Département"
              className="w-80"
            />
            <button
              disabled={isSubmitting || !isValid}
              type="submit"
              className="bg-yellow active:bg-dark-yellow disabled:opacity-50 fond-semibold px-4 py-2 mt-5 rounded-lg"
            >
              Ajouter
            </button>
            {err && <ErrorParagraph messageError={err} />}
          </Form>
        )}
      </Formik>
    </Layout>
  )
}

export default AddEmployee

import Layout from "@/components/Layout"
import Head from "next/head"
import Login from "./login"

const IndexPage = () => {
  return (
    <main>
      <Head>
        <title>BricoPlus</title>
      </Head>
      <Login />
    </main>
  )
}

IndexPage.isPublic = true
export default IndexPage

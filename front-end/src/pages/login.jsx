import ErrorParagraph from "@/components/Error"
import FormField from "@/components/FormField"
import useAppContext from "@/hooks/useAppContext"
import parseSession from "@/utils/parseSession"
import { Form, Formik } from "formik"
import Image from "next/image"
import { useRouter } from "next/router"
import { useCallback, useEffect, useState } from "react"
import * as yup from "yup"

const Login = () => {
  const [err, setErr] = useState(null)
  const router = useRouter()
  const {
    actions: { login },
    state: { cookies },
  } = useAppContext()

  useEffect(() => {
    if (cookies.session) {
      const employeeId = parseSession(cookies.session).id
      const role = parseSession(cookies.session).roles[0].authority
      if (role === "employee") {
        router.push(`/${employeeId}/pointing`)
      } else {
        router.push("/rhview")
      }
    }
  })

  const initialValues = {
    email: "",
    password: "",
  }

  const validationSchema = yup.object().shape({
    email: yup.string().required("Email manquant").email("Email invalide"),
    password: yup
      .string()
      .required("Password manquant")
      .min(12, "Le mot de passe doit faire au moins 12 caractères")
      .matches(
        /[a-z]/,
        "Le mot de passe doit contenir au moins une lettre minuscule"
      )
      .matches(
        /[A-Z]/,
        "Le mot de passe doit contenir au moins une lettre majuscule"
      )
      .matches(/[0-9]/, "Le mot de passe doit contenir au moins un chiffre")
      .matches(
        /[@$!%*#?&]/,
        "Le mot de passe doit contenir au moins un caractère spécial"
      ),
  })

  const handleSubmit = useCallback(
    async ({ email, password }) => {
      setErr(null)
      const [error] = await login(email, password)

      if (error) {
        setErr(error)

        return
      }

      const employeeId = parseSession(cookies.session).id
      const role = parseSession(cookies.session).roles[0].authority
      if (role === "employee") {
        router.push(`/${employeeId}/pointing`)
      } else {
        router.push("/rhview")
      }
    },
    [router]
  )

  return (
    <main className="flex items-center">
      <div className="bg-yellow w-[662px] h-screen relative">
        <div className="bg-dark-yellow w-[555px] h-[794px] absolute right-0 inset-y-0 m-auto px-auto">
          <Image
            src="/name.png"
            alt="name"
            width={362}
            height={186}
            className="right-[80px] absolute mt-[350px]"
          />
        </div>
      </div>
      <Image
        src="/logo.png"
        alt="logo"
        width={196}
        height={175}
        className="absolute mt-[670px] ml-[525px]"
      />
      <div className="bg-dark-grey grow h-[794px] flex flex-col items-center justify-center">
        <h1 className="text-[48px] font-bold mb-[50px]">Se connecter</h1>
        <Formik
          initialValues={initialValues}
          onSubmit={handleSubmit}
          validationSchema={validationSchema}
          error={err}
        >
          {({ isSubmitting, isValid }) => (
            <Form className="flex flex-col">
              <FormField
                name="email"
                placeholder="Email"
                className="w-[600px] text-[24px]"
                classNameInput="h-[75px] px-[16px] border-none"
              />
              <FormField
                name="password"
                placeholder="Mot de passe"
                className="w-[600px] text-[24px]"
                classNameInput="h-[75px] px-[16px] mt-[76px] border-none"
                type="password"
              />
              <button
                disabled={isSubmitting || !isValid}
                type="submit"
                className="bg-yellow active:bg-dark-yellow disabled:opacity-50 px-4 py-2 mt-[80px] w-[600px] h-[74px] text-[40px]"
              >
                Se connecter
              </button>
              {err && <ErrorParagraph messageError={err} />}
            </Form>
          )}
        </Formik>
      </div>
    </main>
  )
}

Login.isPublic = true
export default Login

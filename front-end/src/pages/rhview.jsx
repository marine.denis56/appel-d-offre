import AllEmployeeArray from "@/components/AllEmployeeArray"
import EmployeeArray from "@/components/EmployeeArray"
import Layout from "@/components/Layout"
import { createApi } from "@/services/api"
import { getEmployeesByDateService } from "@/services/employee"
import Image from "next/image"
import { useRouter } from "next/router"
import { useCallback, useEffect } from "react"
import errorHandler from "@/utils/errorHandler"
import useAppContext from "@/hooks/useAppContext"
import parseSession from "@/utils/parseSession"

export const getServerSideProps = async (context) => {
  const api = createApi({ jwt: context.req.cookies.session })

  const getEmployees = getEmployeesByDateService({ api, errorHandler })
  const [error, data] = await getEmployees(
    new Date().toISOString().slice(0, 10)
  )

  if (error) {
    return { props: { error: error } }
  }

  return {
    props: { employees: data },
  }
}

const RHView = (props) => {
  const { employees } = props

  const options = {
    weekday: "long",
    month: "long",
    day: "numeric",
  }
  const now = new Date().toLocaleDateString(undefined, options)
  const {
    state: { cookies },
  } = useAppContext()

  const router = useRouter()

  const handleClick = useCallback(() => {
    router.push(`/addEmployee`)
  }, [router])

  const lateList = employees.filter((employee) => employee.status === "late")
  const onTimeList = employees.filter(
    (employee) => employee.status === "onTime"
  )
  const absentList = employees.filter(
    (employee) => employee.status === "absent"
  )

  useEffect(() => {
    if (parseSession(cookies.session).roles[0].authority === "employee") {
      router.push(`/${parseSession(cookies.session).id}/pointing`)
    }
  })

  return (
    <Layout>
      <p className="absolute pl-[75px] pt-[80px] text-base font-bold">Vue RH</p>
      <div className="pt-[150px] flex justify-center">
        <p className="text-yellow font-bold text-5xl">BONJOUR</p>
        <Image src="/clock.png" width={50} height={50} alt="clock" />
      </div>
      <p className="text-center font-bold text-[32px] pb-[70px]">{now}</p>
      <button
        onClick={handleClick}
        className="absolute right-[90px] bg-yellow font-semi-bold px-[20px] py-[15px] rounded-xl"
      >
        Ajouter un<br></br>employee
      </button>
      <AllEmployeeArray
        late={lateList ? lateList.length : 0}
        onTime={onTimeList ? onTimeList.length : 0}
        absent={absentList ? absentList.length : 0}
      />
      <EmployeeArray allEmployees={employees} />
    </Layout>
  )
}

export default RHView

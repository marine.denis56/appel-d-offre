export const getEmployeeService =
  ({ api, errorHandler }) =>
  async (employeeId) => {
    try {
      const { data } = await api.get(`/employee/${employeeId}`)

      return [null, data]
    } catch (err) {
      return errorHandler(err)
    }
  }

export const postEmployeeStatusService =
  ({ api, errorHandler }) =>
  async (employeeId, date, timeIn) => {
    try {
      const { data } = await api.post("/employee/status/create", {
        idEmployee: employeeId,
        date: date,
        timeIn: timeIn,
      })

      return [null, data]
    } catch (err) {
      return errorHandler(err)
    }
  }

export const getEmployeesByDateService =
  ({ api, errorHandler }) =>
  async (date) => {
    try {
      const { data } = await api.get(`/employee/attendances?date=${date}`)
      return [null, data]
    } catch (err) {
      return errorHandler(err)
    }
  }

export const createEmployeeService =
  ({ api, errorHandler }) =>
  async (
    firstName,
    lastName,
    gender,
    role,
    departement,
    phone,
    email,
    password
  ) => {
    try {
      const { data } = await api.post("/register", {
        firstName: firstName,
        lastName: lastName,
        gender: gender,
        role: role,
        departement: departement,
        phone: phone,
        email: email,
        password: password,
      })
      return [null, data]
    } catch (err) {
      return errorHandler(err)
    }
  }

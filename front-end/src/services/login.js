import { createApi } from "./api"

export const loginService =
  ({ api, setCookie, errorHandler }) =>
  async (email, password) => {
    try {
      const { data } = await api.post("/auth/signin", {
        email,
        password,
      })

      setCookie("session", data, { path: "/" })

      return [null, true]
    } catch (err) {
      return errorHandler(err)
    }
  }

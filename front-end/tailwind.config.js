/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
    },
    colors: {
      "dark-yellow": "#F6B100",
      yellow: "#FFC514",
      "dark-grey": "#E9E7E3",
      grey: "#F6F6F6",
      red: "#DF1C1C",
      green: "#77CC0A",
      white: "#FFFFFF",
      black: "#000000",
    },
    fontFamily: {
      montserrat: ["Montserrat"],
    },
  },
  plugins: [],
}
